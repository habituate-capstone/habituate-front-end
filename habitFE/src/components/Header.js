import * as React from 'react';
import {Text, View, StyleSheet, AppRegistry} from 'react-native';
import styles from './styles.js';

export default class Header extends React.Component {
    render() {
        return (
            <View style={styles.header}>
                <Text style={styles.headerText}>HabitApp</Text>
            </View>
        )
    }
}

AppRegistry.registerComponent('HabitFE', () => Header);