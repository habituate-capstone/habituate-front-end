import * as React from 'react';
import { Alert, 
    AppRegistry, 
    Image, 
    Platform, 
    ProgressBarAndroid, 
    ProgressViewIOS, 
    StyleSheet, 
    Text, 
    TouchableHighlight, 
    TouchableNativeFeedback, 
    View,
    Navigator 
} from 'react-native';

import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { SCLAlert, SCLAlertButton } from 'react-native-scl-alert'
import styles from './styles.js';

export default class HabitPanel extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            habitProgress: props.habitProgress, // capture habitProgress prop to be incremented by taps on panel
            progressVal: (props.habitProgress / props.habitGoal), // used for progress bar
            showOptions: false, // used to show/hide the edit/delete buttons for the panel
            showDelete: false, // used to show/hide the delete "are you sure?" alert
            gestureName: 'none', // used by GestureRecognizer
        };
    }

    // close delete SCLAlert prompt
    closeDeleteAlert = () => {
        this.setState({ showDelete: false })
    }

    deleteHabit = () => {
        console.log("DELETING");
        this.props.deleteHabit(this.props.habitId, this.props.userId);
        this.closeDeleteAlert();
    }

    determineIcon(icon) {
        var path = null;
        switch(icon){
            case 'Food':
            case 'food':
            return 0;

            case 'Fitness':
            case 'fitness':
            return 1;

            case 'Studying':
            case 'studying':
            return 2;

            case 'Relationships':
            case 'relationship':
            return 3;

            default:
            return 4;
        }

    }

    // open delete SCLAlert prompt
    openDeleteAlert = () => {
        this.setState({ showDelete: true })
    }

    // increment the progress if it is currently less than the daily goal
    // worth discussing - do we want to track activity beyond the goal?
    onPressUpdate = () => {
        if(this.state.habitProgress < this.props.habitGoal)
        {
             newProgress = this.state.habitProgress + 1;
             encodedUser = encodeURIComponent(this.props.userId);
             encodedHabit = encodeURIComponent(this.props.habitId);

             fetch(`https://habitapp-back-end-prod.cfapps.io/habits/${encodedUser}/${encodedHabit}/update`,
             {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "habitProgress": newProgress
                })
             })
             .then((response) => {
                console.log(response);
             })
             .catch((error) =>  {
                console.error(error);
             });
            this.setState({ habitProgress: newProgress }, () => this.updateProgressBar());
        }
    }

    // show delete and edit buttons on a left swipe
     onSwipeLeft(gestureState) {
        this.setState({showOptions: true});
     }

    // hide delete and edit buttons on a right swipe
     onSwipeRight(gestureState) {
        this.setState({showOptions: false});
     }

    // update the progress bar's decimal value with the new state value (callback function in onPressUpdate)
    updateProgressBar = () => {
        this.setState({ progressVal: this.state.habitProgress / this.props.habitGoal})
    }

    render() {
        // configuration values for GestureRecognizer
        const config = {
            velocityThreshold: 0.3,
            directionalOffsetThreshold: 80
        };

        const iconImages = [
                  require('../assets/images/icons/food.png'),
                  require('../assets/images/icons/yoga-xxl.png'),
                  require('../assets/images/icons/study.png'),
                  require('../assets/images/icons/medical.png'),
                  require('../assets/images/icons/running-xxl.png')
        ];

        if(!this.state.showOptions)
        {
            // main panel view
            return (
                <GestureRecognizer
                    onSwipeLeft={(state) => this.onSwipeLeft(state)}
                    config={config}>
                        <TouchableNativeFeedback onPress={this.onPressUpdate}>
                            <View style={styles.habitPanel}>
                                {/* <Image source={this.props.iconPath} style={styles.icon}/> */}
                                <Image source={iconImages[this.determineIcon(this.props.habitCategory)]} style={styles.icon}/>
                                <View style={styles.habitDetails}>
                                    <Text style={styles.habitText}>{this.props.habitName}</Text>
                                    {
                                        // use progress bar specific to OS - for if we decide to deploy on iOS also
                                        (Platform.OS === 'android')
                                        ?
                                            (<ProgressBarAndroid styleAttr = "Horizontal" progress = {this.state.progressVal} color = "#439dbb" indeterminate = {false}/>)
                                        :
                                            (<ProgressViewIOS progress = {this.state.progressVal} trackTintColor = "#439dbb"/>)
                                    }
                                    <View style={styles.habitSubDetails}>
                                        <Text style={styles.habitText}>{this.state.habitProgress}/{this.props.habitGoal}</Text>
                                        <Text style={styles.habitText}>{this.props.habitGoal} per {this.props.habitFrequency}</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableNativeFeedback>
                 </GestureRecognizer>
            )
        }

        // options (edit and delete) panel view
        return (
            <GestureRecognizer
                onSwipeRight={(state) => this.onSwipeRight(state)}
                config={config}>
                    <View style={styles.habitPanelOptions}>
                        <SCLAlert
                            theme="default"
                            show={this.state.showDelete}
                            title="Delete Habit"
                            subtitle="Are you sure? This cannot be undone."
                            onRequestClose={(state) => this.closeDeleteAlert(state)}
                            titleStyle={styles.deleteHeader}
                            headerIconComponent=<Image source={require("../assets/images/delete-xxl.png")} style={styles.icon}/>
                            backgroundColor="#439dbb"
                         >
                            <SCLAlertButton
                                theme="info"
                                onPress={this.closeDeleteAlert}
                                textStyle={styles.deleteHeader}
                                containerStyle={styles.deleteButton}
                            >
                                Cancel
                            </SCLAlertButton>
                            <SCLAlertButton
                                theme="info"
                                onPress={this.deleteHabit}
                                textStyle={styles.deleteHeader}
                                containerStyle={styles.deleteButton}
                            >
                                Delete
                            </SCLAlertButton>
                         </SCLAlert>

                        <TouchableNativeFeedback onPress={this.openDeleteAlert}>
                            <View>
                                <Image source={require("../assets/images/delete-xxl.png")} style={styles.icon}/>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback onPress={() => {
                            this.setState({showOptions: false});
                            this.props.navigation.navigate('Edit',
                                {
                                    name: this.props.habitName,
                                    goal: this.props.habitGoal,
                                    frequency: this.props.habitFrequency,
                                    category: this.props.habitCategory,
                                    habitId: this.props.habitId,
                                    userId: this.props.userId,
                                    update: this.props.update
                                });

                            }
                        }>
                            <View>
                                <Image source={require("../assets/images/edit-2-xxl.png")} style={styles.icon}/>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
             </GestureRecognizer>
        )
    }

}

AppRegistry.registerComponent('HabitFE', () => HabitPanel);