import React, {Component} from 'react';
import { Image, Text, View } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import type { User } from 'react-native-google-signin';
import HabitScreen from './HabitScreen.js';
import AddHabit from './AddHabit';
import EditHabit from './EditHabit';
import Header from './Header'
import styles from './styles.js';
import config from './config';

type ErrorWithCode = Error & { code?: string };

type State = {
    error: ?ErrorWithCode,
    userInfo: ?User
}

export default class Home extends React.Component {

    state = {
        userInfo: null,
        error: null,
        update: 0
    }

    async componentDidMount() {
        this.configureGoogleSignIn();
        await this.getCurrentUser();
    }

    componentDidUpdate(){
        if(this.props.navigation.getParam('update', 0) !== this.state.update)
        {
            this.setState({update: this.props.navigation.getParam('update')});
            console.log(this.state.update);
        }
    }

    configureGoogleSignIn() {
        GoogleSignin.configure();
    }

    async getCurrentUser() {
        try {
            const userInfo = await GoogleSignin.signInSilently();
            this.setState({ userInfo, error: null });
        } catch (error) {
            const errorMessage = error.message;
            this.setState({
                error: new Error(errorMessage)
            });
        }
    }

    render() {

        const { userInfo } = this.state;

        console.log(userInfo);

        const body = userInfo ? this.renderHabitScreen(userInfo) : this.renderAuthScreen();
        return (
            <View style={styles.container} navigation={this.props.navigation}>
                {body}
            </View>
        )
    }

    renderHabitScreen() {
        return (
            <HabitScreen userInfo={this.state.userInfo} navigation={this.props.navigation} update={this.state.update}/>
        )
    }

    renderAuthScreen() {
        return (
            <View style={styles.container} navigation={this.props.navigation}>
                <Header/>
                <View style={styles.authContainer}>
                    <Image style={{flexBasis: 200, resizeMode:"contain", marginBottom: 50}} source={require("../assets/images/check-mark-11-256.png")}/>
                    <View>
                        <Text style={styles.authText}>Welcome to HabitApp!</Text>
                        <Text style={styles.authText}>Log in below to track your habits:</Text>
                    </View>
                    <View navigation={this.props.navigation} style={{marginTop:50}}>
                        <GoogleSigninButton
                            style={{width: 312, height: 48}}
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Light}
                            onPress={this.signIn}
                        />
                    </View>
                </View>
            </View>
        )
    }

    signIn = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            this.setState({ userInfo });
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log("Sign in Cancelled");
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log("In Progress");
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.log("Play services not available");
            } else {
                console.log(error);
            }
        }
    };

}