/*NOTE -
THIS PAGE WILL BE UPDATED TO EDIT ONCE 
WE CAN PULL EXISTING DATA FROM THE DB*/

import React, { Component } from 'react';
import { Alert, View, TextInput, Picker, TouchableOpacity, Text} from 'react-native';
import Header from './Header';
import styles from './styles';


const { input,
    bottom, 
    viewStyle, 
    formViewStyle,
    bottomButtonStyle, 
    frequencyStyle, 
    pickerStyle, 
    buttonStyle, 
    buttonContainer 
} = styles;

class AddHabit extends Component {
    constructor(props){
        super(props);
        this.state = {
            goal: this.props.navigation.getParam('goal').toString(),
            category: this.props.navigation.getParam('category'),
            name: this.props.navigation.getParam('name'),
            frequency: this.props.navigation.getParam('frequency'),
            type: '',    
        };
    }

    updateGoal = (goal) => {
        this.setState({goal: goal})
    }
    updateCategory = (category) => {
        this.setState({category: category})
    }
    updateName = (name) => {
        this.setState({name: name})
    }
    updateFrequency = (frequency) => {
        this.setState({frequency: frequency})
    }

    formatGoal = (goal) => {
        let formatedGoal = '';
        switch(goal){
            case "day":
            case "days":
                formatedGoal = "day";
                return formatedGoal;

            case "week":
            case "weeks":
                formatedGoal = "week";
                return formatedGoal;

            case "month":
            case "months":
                formatedGoal = "month";
                return formatedGoal;

            case "year":
            case "years":
                formattedGoal = "year";
                return formatedGoal;

        }
    }

    editHabit = (habitName, frequency, goal, category, userId, habitId) => {

        newGoal = this.formatGoal(frequency);
        console.log("habitname: " + habitName);
        console.log("timeUnit: " + frequency);
        console.log("goal: " + goal);
        console.log("new goal: " + newGoal);
        console.log("category: " + category);
        console.log("userId: " + userId);
        console.log("habitId: " + habitId);

        if(goal < 1) {
            Alert.alert(
                'Habit\'s must have a goal of at least 1!'
            )
            this.props.navigation.navigate('Edit');
        }
        else {
            //Post request to back end
            fetch('https://habitapp-back-end-prod.cfapps.io/habits/' + userId + '/' + habitId + '/update',
                {
                    method: 'PUT',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',

                    },
                    body: JSON.stringify({
                        "habitName" : habitName,
                        "frequency" : newGoal,
                        "habitGoal" : parseInt(goal, 10),
                        "timeUnit" : newGoal,
                        "habitCategory" : category
                    })
                })
                .then((response) => {
                    console.log(response);
                    this.props.navigation.navigate('Home', {
                        update: this.props.navigation.getParam('update', 0) + 1
                    });
                })
                .catch((error) =>  {
                    console.error(error);
                });
        }
    }
    render() {
        const name = this.props.navigation.getParam('name');
        const goal = this.props.navigation.getParam('goal').toString();
        const frequency = this.props.navigation.getParam('frequency');
        const category = this.props.navigation.getParam('category');
        const habitId = this.props.navigation.getParam('habitId');
        const currentUser = this.props.navigation.getParam('userId');

 /*       this.updateName(name);
        this.updateGoal(goal);
        this.updateFrequency(frequency);
        this.updateCategory(category);*/

        return (
           <View style={viewStyle}>
                <Header />
                <View style={formViewStyle}>
                <TextInput style={input} placeholder='Habit Name' placeholderTextColor='white' value={this.state.name} defaultValue={name} onChangeText={this.updateName}/>

                    <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                    <TextInput style={frequencyStyle} placeholder='Habit Frequency' placeholderTextColor='white' keyboardType='numeric' value={this.state.goal} defaultValue={goal} onChangeText={this.updateGoal}/>

                    <Picker style={pickerStyle} selectedValue = {this.state.frequency} onValueChange = {this.updateFrequency}>
                            <Picker.Item label={frequency} value={frequency}/>
                            <Picker.Item label = "Day" value = "days" />
                            <Picker.Item label = "Week" value = "weeks" />
                            <Picker.Item label = "Month" value = "months" />
                            {/* <Picker.Item label = "Year" value = "years" /> */}
                        </Picker>
                    </View>

                    <Picker style={{color: 'white', backgroundColor: '#717171', height: 40}} selectedValue = {this.state.category} onValueChange = {this.updateCategory}>
                            <Picker.Item label = {category} value = {category} />
                            <Picker.Item label = "Food" value = "food" />
                            <Picker.Item label = "Fitness" value = "fitness" />
                            <Picker.Item label = "Studying" value = "studying" />
                            <Picker.Item label = "Relationship" value = "relationship" />
                    </Picker>

                </View>
                <View style={bottomButtonStyle}>
                    <TouchableOpacity style={buttonContainer} onPress={() => this.editHabit(this.state.name, this.state.frequency, this.state.goal, this.state.category, currentUser, habitId )}>
                        <Text style={buttonStyle}>Save Edit</Text>
                    </TouchableOpacity>
                 </View>
                
           </View>
          
        );  
    }
}


export default AddHabit;


