import * as React from "react";
import { FlatList, View } from "react-native";
import styles from './styles.js';
import HabitPanel from './HabitPanel.js';

export default class Habits extends React.Component {

    // render a list of habit panels, one for each item of array passed in via habitsData prop
    render() {
        return(
            <View style={{flex:5}}>
                <FlatList
                    data={this.props.habitsData}
                    renderItem={({item}) => <HabitPanel habitName={item.name} habitProgress={item.progress} habitGoal={item.goal} habitFrequency={item.frequency} habitCategory={item.category} iconPath={item.iconPath} navigation={this.props.navigation}/>}
                />
            </View>

        )
    }

}