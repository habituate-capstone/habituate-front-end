
import React, {Component} from 'react';
import {Button, FlatList, Image, Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Header from './Header.js';
import HabitPanel from './HabitPanel.js';
import HabitList from './HabitList.js'
import styles from './styles.js';

export default class HabitScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            habitsData: [],
            userInfo: this.props.userInfo,
            userId: this.props.userInfo.user.id,
            userEmail: this.props.userInfo.user.email,
            userName: this.props.userInfo.user.name,
            update: this.props.update
        }
    }

    componentDidMount(){
        console.log(this.state.update);
        this.getHabits();
    }

    componentDidUpdate(){
        if(this.props.update !== this.state.update)
        {
            this.setState({update: this.props.update});
            this.getHabits();
        }
    }

    deleteHabit = (habitId, userId) => {
        encodedUser = encodeURIComponent(userId);
        encodedHabit = encodeURIComponent(habitId);
        fetchString = `https://habitapp-back-end-prod.cfapps.io/habits/`;

        fetch(`https://habitapp-back-end-prod.cfapps.io/habits/${encodedUser}/${encodedHabit}`,
                {
                    method: 'DELETE',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json'
                    }
                })
                .catch((error) =>  {
                    console.error(error);
                });

        this.getHabits();
    }

    getHabits = () => {
        this.setState({habitsData: []});
        fetch('https://habitapp-back-end-prod.cfapps.io/users/save',
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "userId": this.state.userId,
                    "email": this.state.userEmail,
                    "name": this.state.userName
                })
            })
            .then((response) => {
                console.log(response);
                fetch('https://habitapp-back-end-prod.cfapps.io/habits',
                {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'userId': this.state.userId
                    }
                })
                .then((response) => response.json())
                .then((responseJson) =>
                {
                    for(i = 0; i < responseJson.length; i++)
                    {
                        this.setState((state) => {
                           item = responseJson[i];
                            list = state.habitsData.concat(
                            {
                                key: item.habitId,
                                name: item.habitName,
                                progress: item.habitProgress,
                                goal: item.habitGoal,
                                frequency: item.timeUnit,
                                category: item.habitCategory,
                            });
                            return {
                                habitsData: list
                            };
                        });

                    }
                })
                .catch((error) =>  {
                    console.error(error);
                });
            })
            .catch((error) =>  {
                console.error(error);
            });
    }


    render() {
        if(this.state.habitsData.length == 0)
        {
            return (
                <View style={styles.container}>
                    <Header/>
                    <View style={styles.authContainer}>
                        <Text style={styles.authText}>
                            It looks like you aren&apos;t tracking any habits right now! Click below to add a habit to your list!
                        </Text>
                    </View>
                    <View style={styles.bottom}>
                        <Button color="#439dbb" title="Add new habit" onPress={() => this.props.navigation.push('Add', {
                            currentUser: this.props.userInfo,
                            //habitUpdate: this.getHabits
                        })}/>
                    </View>
                </View>

            );



        }

        return (
            <View style={styles.container}>
                <Header/>
                <View style={{flex:5}}>
                    <FlatList
                        data={this.state.habitsData}
                        renderItem={({item}) => <HabitPanel
                            userId={this.state.userId}
                            habitId={item.key}
                            habitName={item.name}
                            habitProgress={item.progress}
                            habitGoal={item.goal}
                            habitFrequency={item.frequency}
                            habitCategory={item.category}
                            iconPath={item.iconPath}
                            navigation={this.props.navigation}
                            deleteHabit = {this.deleteHabit}
                            update = {this.props.update}/>}
                    />
                </View>
                <View style={styles.bottom}>
                    <Button color="#439dbb" title="Add new habit" onPress={() => this.props.navigation.push('Add', {
                            currentUser: this.props.userInfo,
                            update: this.props.update
                        })}/>
                </View>
            </View>
        );
    }
}