import { StyleSheet } from 'react-native';

export default StyleSheet.create({

    authCenter: {
        height: 200,
        backgroundColor: "#439dbb",
        flexDirection: "column",
        alignItems: "center",
    },
    authContainer: {
        flex: 1,
        justifyContent: "space-around",
        alignItems: "center",
        backgroundColor: "#383838",
        marginTop: 100,
        marginBottom: 100,
        marginLeft: 50,
        marginRight: 50
    },
    authText: {
        color: "white",
        fontFamily: "Montserrat-Bold",
        fontSize: 20,
        textAlign: "center",
        marginBottom: 10
    },
    bottom: {
        flex: 1,
        justifyContent: "center",
        alignItems:  "center",
        alignSelf: "center",
        width: 300,
    },
    container: {
        flex: 1,
        justifyContent: "flex-start",
        backgroundColor: "#383838"
    },
    deleteButton: {
        backgroundColor: "#439dbb"
    },
    deleteHeader: {
        fontFamily: "Montserrat-Bold",
    },
    deleteText: {
        fontFamily: "Montserrat-Light",
        fontSize: 10
    },
    habitDetails: {
        flex: 1,
        margin: 10,
        flexDirection: "column",
        justifyContent: "center"
    },
    habitPanel: {
      height: 100,
      backgroundColor: "#717171",
      margin: 15,
      flexDirection: "row",
      alignItems: "center"
    },
    habitPanelOptions: {
        height: 100,
        backgroundColor: "#439dbb",
        margin: 15,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around"
    },
    habitSubDetails: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    habitText: {
        color: "white",
        fontFamily: "Montserrat-Light",
        fontSize: 15
    },
    header: {
        height: 100,
        backgroundColor: "#439dbb",
        flexDirection: "row",
        justifyContent:  "flex-end",
        alignItems: "flex-end"
    },
    headerText: {
        flex: 1,
        fontFamily: "Montserrat-Light",
        color: "#383838",
        fontSize: 50,
        textAlign: "right",
        bottom: 5,
        right: 5
    },
    icon: {
        flexBasis: 50,
        resizeMode: "contain"
    },
    input: {
        height: 40,
        backgroundColor: '#717171',
        marginBottom: 20,
        marginTop: 10,
        color: 'white',
        paddingHorizontal: 10
    },
    frequencyStyle: {
        height: 40,
        width: 242,
        backgroundColor: '#717171',
        marginBottom: 20,
        color: 'white',
        paddingHorizontal: 10
    },
    pickerStyle: {
        height: 40,
        //width: 150,
        width: '37.5%',
        marginBottom: 20,
        color: 'white',
        backgroundColor: '#717171',
        paddingHorizontal: 10
    },
    typePickerStyle: {
        height: 40,
        marginBottom: 20,
        color: 'white',
        backgroundColor: '#717171',
        paddingHorizontal: 10
    },
    viewStyle: {
        backgroundColor: '#383838',
        flex: 1
    },
    formViewStyle: {
        padding: 10,
        justifyContent: 'center',
        flex: 1
    },
    buttonStyle: {
        textAlign: 'center',
        color: '#000'
    },
    buttonContainer: {
        backgroundColor: '#439dbb',
        paddingVertical: 10,
        marginTop: 20,
        justifyContent: 'flex-end'
    },
    bottomButtonStyle: {
        //flex: 1,
        backgroundColor: '#383838',
       // backgroundColor: 'pink',
        justifyContent: 'flex-end',
        marginBottom: 20,
        paddingHorizontal: 10
    }


})