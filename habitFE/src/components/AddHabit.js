import React, { Component } from 'react';
import { Alert, View, TextInput, Picker, TouchableOpacity, Text} from 'react-native';
import Header from './Header';
import styles from './styles';
import moment from 'moment';



let TEST_USER_ID = '101202513865343945908';

const { input,
    bottom, 
    viewStyle, 
    formViewStyle,
    bottomButtonStyle, 
    frequencyStyle, 
    pickerStyle, 
    buttonStyle, 
    buttonContainer 
} = styles;

class AddHabit extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            goal: '',
            type: '',
            frequency: '',
            currentDate: new Date(),
            markedDate: moment(new Date()).format("YYYY-MM-DD")  
        };
    }
    
    updateGoal = (goal) => {
        this.setState({goal: goal})
    }
    updateType = (type) => {
        this.setState({type: type})
    }
    updateName = (name) => {
        console.log("Changed state!");
        this.setState({name: name})
    }
    updateFrequency = (frequency) => {
        this.setState({frequency: frequency})
    }

    addNewHabit = (habitName, frequency, goal, category, currentUser) => {
        //calculate the end date based on the frequency of the habit
        startDate = this.formatCurrentDate(goal);
        endDate = this.formatEndDate(goal, startDate);
        newGoal = this.formatGoal(goal);
       


        if(frequency < 1) {
            Alert.alert(
                'Habit\'s must have a goal of at least 1!'
            )
            this.props.navigation.navigate('Add');
        }
        else {
            //Post request to back end
            fetch('https://habitapp-back-end-prod.cfapps.io/habits',
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'userId': currentUser.user.id
                    },
                    body: JSON.stringify({
                        "userId" : currentUser.user.id,
                        "habitName" : habitName,
                        "startDate" : startDate,
                        "endDate" : endDate,
                        "frequency" : newGoal,
                        "habitGoal" : parseInt(frequency, 10),
                        "timeUnit" : newGoal,
                        "habitProgress" : 0,
                        "habitCategory" : category
                    })
                })
                .then((response) => {
                    //habitUpdate = this.props.navigation.getParam('habitUpdate');
                    //habitUpdate();
                    this.props.navigation.navigate('Home', {
                        update: this.props.navigation.getParam('update', 0) + 1
                    });
                })
                .catch((error) =>  {
                    console.error(error);
                });
        }
}

    //functions to calculate the end date based on habit frequency
    formatCurrentDate = (frequency) =>{   
        const today = this.state.currentDate;
        const currentDate = moment(today).format("YYYY-MM-D");
        return currentDate;
    }

    formatEndDate = (frequency, currentDate) => {
        const today = this.state.currentDate;
        const endDate = moment(today).add(1, frequency);
        const format = moment(endDate._d).format("YYYY-MM-DD");
        return format;

    }

    formatGoal = (goal) => {
        let formatedGoal = '';
        switch(goal){
            case "days":
                formatedGoal = "day";
                return formatedGoal;

            case "weeks":
                formatedGoal = "week";
                return formatedGoal;
            
            case "months":
                formatedGoal = "month";
                return formatedGoal;

            case "years":
                formattedGoal = "year";
                return formatedGoal;

        }
    }

    render() {
        const currentUser = this.props.navigation.getParam('currentUser');
        return (
           <View style={viewStyle}>
                <Header />
                <View style={formViewStyle}>
                    <TextInput style={input} placeholder='Habit Name' placeholderTextColor='white' value={this.state.name} onChangeText={this.updateName}/>
                    <View style={{flexDirection: 'row', justifyContent: 'center'}}>
                    <TextInput style={frequencyStyle} placeholder='Habit Frequency' placeholderTextColor='white' keyboardType='numeric' value={this.state.frequency} onChangeText={this.updateFrequency}/>
                        
                        <Picker style={pickerStyle} selectedValue = {this.state.goal} onValueChange = {this.updateGoal}>
                            <Picker.Item label="Duration" value={null}/>
                            <Picker.Item label = "Day" value = "days" />
                            <Picker.Item label = "Week" value = "weeks" />
                            <Picker.Item label = "Month" value = "months" />
                            {/* <Picker.Item label = "Year" value = "years" /> */}
                        </Picker>
                    </View>

                    <Picker style={{color: 'white', backgroundColor: '#717171', height: 40}} selectedValue = {this.state.type} onValueChange = {this.updateType}>
                            <Picker.Item label="Habit Type" value={null}/>
                            <Picker.Item label = "Food" value = "food" />
                            <Picker.Item label = "Fitness" value = "fitness" />
                            <Picker.Item label = "Studying" value = "studying" />
                            <Picker.Item label = "Relationship" value = "relationship" />
                    </Picker> 
                   <Text>{this.state.frequency}</Text>
                </View>
                <View style={bottomButtonStyle}>
                    <TouchableOpacity style={buttonContainer} onPress={() => this.addNewHabit(this.state.name, this.state.frequency, this.state.goal, this.state.type, currentUser)}>
                    {/* <TouchableOpacity style={buttonContainer} onPress={() => this.props.navigation.navigate('Home')}></TouchableOpacity> */}
                        <Text style={buttonStyle}>Add Habit</Text>
                    </TouchableOpacity>
                 </View>
                
           </View>
          
        );  
    }
}


export default AddHabit;
