/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { Text, View } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { GoogleSignin } from 'react-native-google-signin';
import HabitScreen from './src/components/HabitScreen.js';
import AddHabit from './src/components/AddHabit';
import EditHabit from './src/components/EditHabit';
import Home from './src/components/Home';
import styles from './src/components/styles.js';

const AppNavigator = createStackNavigator({
        Home: {screen: Home},
        Add: {screen: AddHabit},
        Edit: {screen: EditHabit}
    },
    {
        headerMode: 'none',
        initialRouteName: "Home"
    }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {

    render() {
        return <AppContainer/>
    }

}